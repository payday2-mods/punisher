if string.lower(RequiredScript) == "lib/managers/menumanager" then
	if not _G.Punisher then
		_G.Punisher = _G.Punisher or {}
		Punisher._path = ModPath
		Punisher._mpath = ModPath .. "loc/"
		Punisher._savepath = SavePath .. "Punisher_control.txt"
		Punisher.settings = {
			Toggle = true,
			CheckMods = true,
			CheckSkills = true,
			IngameMark = true,
			Punishment = 1,
		}
		Punisher.Caught = { false, false, false, false }
		Punisher.PUNISHMENTS = {
			[1] = { movement_state = 'arrested', validation_condition = 'mugshot_cuffed' },
			[2] = { movement_state = 'tased', validation_condition = 'mugshot_electrified' },
			[3] = { movement_state = 'downed', validation_condition = 'mugshot_downed' },
		}
		Punisher.MODS = {
			black = {
				"carry stacker",
				"silent assassin",
				"pirate perfection",
				"p3dhack",
				"p3dhack free",
				"dlc unlocker",
				"skin unlocker",
				"p3dunlocker",
				"arsium's weapons rebalance recoil",
				"overkill mod",
				"selective dlc unlocker",
				"the great skin unlock",
				"beyond cheats"
			},
			white = {
				"stop the cheater", "stopthecheater",
				"cheater kicker", "cheaterkicker"
			},
			suspicious = {
				"cheet",
				"cheat",
				"dlc",
				"god",
				"hack",
				"hax",
				"hak",
				"mvp",
				"p3d",
				"pirate",
				"trainer",
				"unlock",
				"x-ray"
			}
		}
	end

	function Punisher:Save()
		local file = io.open(self._savepath, "w+")
		if file then
			file:write(json.encode(self.settings))
			file:close()
		end
	end

	function Punisher:Load()
		local file = io.open(self._savepath, "r")
		if file then
			for id, value in pairs(json.decode(file:read("*all")) or {}) do
				self.settings[id] = value
			end
			file:close()
		end
	end

	function Punisher:Message_Receive(message, level)
		if managers.chat then
			managers.chat:_receive_message(1, "Punisher", message .. ".", level == 1 and Color.yellow or (level == 2 and Color.red or Color.green))
		end
	end

	function Punisher:inChat()
		local value = false
		if managers.hud ~= nil and managers.hud._chat_focus == true then
			value = true
		end
		if managers.menu_component ~= nil and managers.menu_component._game_chat_gui ~= nil and managers.menu_component._game_chat_gui:input_focus() == true then
			value = true
		end
		return value
	end

	--Localization
	Hooks:Add("LocalizationManagerPostInit", "Punisher:Localization", function(loc)
		loc:load_localization_file(Punisher._mpath .. "english.json")
	end)

	--Menu
	Hooks:Add("MenuManagerInitialize", "Punisher:Menu_Init", function(menu_manager)
		Punisher.Toggle = function(self)
			if Punisher:inChat() == false and managers.network._session then
				if Punisher.settings.Toggle == true then
					Punisher.settings.Toggle = false
					Punisher:Save()
					if Utils:IsInHeist() then
						Punisher:Message_Receive(managers.localization:text("punisher_restore"))
					else
						Punisher:Message_Receive(managers.localization:text("punisher_off"))
					end
				else
					Punisher.settings.Toggle = true
					Punisher:Save()
					if Utils:IsInHeist() then
						for peer_id, peer in pairs(managers.network._session._peers) do
							Punisher:Inspect(peer)
						end
						Punisher:Message_Receive(managers.localization:text("punisher_recheck"))
					else
						Punisher:Message_Receive(managers.localization:text("punisher_on"))
					end
				end
			end
		end

		MenuCallbackHandler.punisher_CheckSkills_callback = function(self, item)
			Punisher.settings.CheckSkills = (item:value() == "on")
			Punisher:Save()
		end
		MenuCallbackHandler.punisher_CheckMods_callback = function(self, item)
			Punisher.settings.CheckMods = (item:value() == "on")
			Punisher:Save()
		end
		MenuCallbackHandler.punisher_IngameMark_callback = function(self, item)
			Punisher.settings.IngameMark = (item:value() == "on")
			Punisher:Save()
		end
		MenuCallbackHandler.punisher_Punishment_callback = function(self, item)
			Punisher.settings.Punishment = (item:value() == "on")
			Punisher:Save()
		end
	end)

	function Punisher:Inspect(peer)
		if Punisher.Caught[peer:id()] == false then
			if Punisher.settings.CheckSkills == true then
				Punisher:CheckSkills(peer)
			end
			if Punisher.settings.CheckMods == true then
				Punisher:CheckMods(peer)
			end
		end
	end

	function Punisher:CheckSkills(peer)
		local number = 0
		local sum = 0
		local answer = false
		if peer ~= nil then
			local skills_perk_deck_info = string.split(peer:skills() or "", "-") or {}
			if #skills_perk_deck_info == 2 then
				local skills = string.split(skills_perk_deck_info[1] or "", "_")
				--local perk_deck = string.split(skills_perk_deck_info[2] or "", "_")
				for i = 1, #skills do
					number = tonumber(skills[i])
					sum = sum + number
					if number > 117 then
						answer = true
					end
				end
				if sum > 120 then
					answer = true
				end
				if sum > (tonumber(peer:level() or 0) + 2 * math.floor(tonumber(peer:level() or 0) / 10)) then
					answer = true
				end
			end
		end
		if answer == true then
			Punisher:CheaterCaught(peer, "too many skills")
		end
	end

	function Punisher:CheckMods(peer)
		DelayedCalls:Add("Punisher:CheckMods_" .. tostring(peer:id()), 2, function()
			local user = Steam:user(peer:ip())
			if user and user:rich_presence("is_modded") == "1" or peer:is_modded() then
				Punisher:Message_Receive(peer:name() .. " has mods. Checking...")
				local prob_not_clean = nil
				for i, mod in ipairs(peer:synced_mods()) do
					local mod_mini = string.lower(mod.name)
					local whitelisted = nil

					for _, v in pairs(Punisher.MODS.black) do
						if mod_mini == v then
							Punisher:CheaterCaught(peer, "using the mod: " .. mod.name)
							return
						end
					end

					for _, v in pairs(Punisher.MODS.white) do
						if mod_mini == v then
							whitelisted = true
						end
					end

					if not whitelisted then
						for _, v in pairs(Punisher.MODS.suspicious) do
							if string.find(mod_mini, v) then
								Punisher:Message_Receive(peer:name() .. " is using a mod with a suspicious name (" .. v .. "): " .. mod.name, 1)
								prob_not_clean = true
							end
						end
					end
				end

				if not prob_not_clean then
					Punisher:Message_Receive(peer:name() .. " seems to be clean.")
				end
			else
				Punisher:Message_Receive(peer:name() .. " doesn't seem to have mods.")
			end
		end)
	end

	function Punisher:CheaterCaught(peer, reason)
		if Punisher.Caught[peer:id()] == false then
			Punisher:Message_Receive(managers.localization:text("punisher_message", { Peer = peer:name(), Reason = reason }), 2)
			Punisher.Caught[peer:id()] = true
			Punisher:Cheater_Punish(peer, 1, true)
		end
	end

	function Punisher:Cheater_Punish(peer, interval, validate)
		DelayedCalls:Add("Punisher:Punish_" .. tostring(peer:id()), interval, function()
			if peer == nil or Utils:IsInHeist() ~= true or Punisher.settings.Toggle ~= true or Punisher.Caught[peer:id()] ~= true then
				return
			end
			local player_unit = managers.criminals:character_unit_by_peer_id(peer:id())
			if player_unit == nil then
				return
			end

			local down_time = player_unit:character_damage():down_time()
			if not down_time then
				down_time = 30
			end

			local punishment = Punisher.PUNISHMENTS[Punisher.settings.Punishment or 1]
			local movement_state = punishment.movement_state or "arrested"
			local validation_condition = punishment.validation_condition or "mugshot_cuffed"

			local validating = false
			local character = managers.criminals:character_data_by_unit(player_unit)
			if validate and character and character.panel_id then
				local panel = managers.hud._teammate_panels[character.panel_id]
				if panel and panel.set_punishment then
					validating = true
					panel:set_punishment(validation_condition)
				end
			end

			managers.network:session():send_to_peers_synched("sync_player_movement_state", player_unit, movement_state, down_time, player_unit:id())
			player_unit:movement():sync_movement_state(movement_state, down_time)

			if validating then
				Punisher:Cheater_Punish_Validate(peer, 1)
			end

			Punisher:Cheater_Punish(peer, down_time + 1, validate and not validating)
		end)
	end

	function Punisher:Cheater_Punish_Validate(peer, interval)
		DelayedCalls:Add("Punisher:Punish_Validate_" .. tostring(peer:id()), interval, function()
			if peer == nil or Utils:IsInHeist() ~= true or Punisher.settings.Toggle ~= true or Punisher.Caught[peer:id()] ~= true then
				return
			end
			local player_unit = managers.criminals:character_unit_by_peer_id(peer:id())
			if player_unit == nil then
				return
			end
			local character = managers.criminals:character_data_by_unit(player_unit)
			if character == nil or character.panel_id == nil then
				return
			end
			local panel = managers.hud._teammate_panels[character.panel_id]
			if panel and not panel:is_getting_punished() then
				managers.network:session():send_to_peers("kick_peer", peer:id(), 0)
				managers.network:session():on_peer_kicked(peer, peer:id(), 0)
			end
		end)
	end

	Punisher:Load()
	MenuHelper:LoadFromJsonFile(Punisher._path .. "menu.json", Punisher, Punisher.settings)
elseif string.lower(RequiredScript) == "lib/managers/gameplaycentralmanager" then

	Hooks:PostHook(GamePlayCentralManager, "start_heist_timer", "Punisher:Cheater_lookup", function(self)
		if Punisher.settings.Toggle then
			for peer_id, peer in pairs(managers.network._session._peers) do
				Punisher:Inspect(peer)
			end
		end
	end)

elseif string.lower(RequiredScript) == "lib/network/base/basenetworksession" then

	Hooks:PostHook(BaseNetworkSession, "on_set_member_ready", "Punisher:Cheater_lookup_ready", function(self, peer_id, ready, state_changed, from_network)
		local peer = managers.network:session():peer(peer_id)
		if peer and Utils:IsInHeist() and peer:waiting_for_player_ready() and Punisher.settings.Toggle then
			Punisher:Inspect(peer)
		end
	end)

	Hooks:Add("BaseNetworkSessionOnPeerRemoved", "Punisher:Person_removed", function(peer, peer_id, reason)
		Punisher.Caught[peer:id()] = false
	end)

elseif string.lower(RequiredScript) == "lib/managers/hud/hudteammate" then
	local set_condition_original = HUDTeammate.set_condition

	function HUDTeammate:set_condition(icon_data, text, ...)
		if self._punisher_punishment and self._punisher_punishment == icon_data then
			self._punisher_punished = true
		end
		return set_condition_original(self, icon_data, text, ...)
	end

	function HUDTeammate:set_punishment(icon_data)
		self._punisher_punishment = icon_data
	end

	function HUDTeammate:is_getting_punished()
		return self._punisher_punished
	end

elseif string.lower(RequiredScript) == "lib/managers/hudmanagerpd2" then
	local mark_cheater_original = HUDManager.mark_cheater

	function HUDManager:mark_cheater(peer_id, ...)
		mark_cheater_original(self, peer_id, ...)
		managers.network:session():peer(peer_id):on_mark_cheater()
	end

elseif string.lower(RequiredScript) == "lib/network/base/networkpeer" then
	local mark_cheater_original = NetworkPeer.mark_cheater

	function NetworkPeer:mark_cheater(reason, auto_kick, ...)
		mark_cheater_original(self, reason, auto_kick, ...)
		self:on_mark_cheater()
	end

	function NetworkPeer:on_mark_cheater()
		if Utils:IsInHeist() and Punisher.settings.Toggle and Punisher.settings.IngameMark and self:waiting_for_player_ready() then
			Punisher:CheaterCaught(self, "the ingame system")
		end
	end

end
